import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {LogoutComponent} from './components/logout/logout.component';
import {JpImagePreloadModule, JpPreloadService} from '@jaspero/ng-image-preload';
import {AuthRoutingModule} from './auth-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './components/register/register.component';
import {PasswordComponent} from './components/password/password.component';
import {ProfileComponent} from './components/profile/profile.component';

@NgModule({
    declarations: [
        LoginComponent,
        LogoutComponent,
        RegisterComponent,
        PasswordComponent,
        ProfileComponent
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        JpImagePreloadModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        JpPreloadService
    ]
})
export class AuthModule {
}
