import {Component, OnDestroy, OnInit} from '@angular/core';
import {JpPreloadService} from '@jaspero/ng-image-preload';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import UserCredential = firebase.auth.UserCredential;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject<void>();
    fieldSelected: string;
    loading = false;
    submitted = false;
    loginForm: FormGroup;
    errorMessages = {
        required: 'This field is required',
        email: 'Invalid E-Mail'
    };

    constructor(
        private preloadService: JpPreloadService,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private toastr: ToastrService
    ) {
    }

    ngOnInit(): void {
        this.preloadService.initialize();

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    onSubmit(): void {
        const formFields = this.loginForm.controls;
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        this.authService.login(formFields.email.value, formFields.password.value)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(this.loginSuccessHandler.bind(this), this.loginErrorHandler.bind(this), () => {
                this.loading = false;
                this.submitted = true;
            });
    }

    private loginSuccessHandler(creds: UserCredential): void {
        setTimeout(() => this.toastr.success('You are successfully logged in', 'Login'), 1);
        this.router.navigate(['/']);
    }

    private loginErrorHandler(error: Error): void {
        console.log(error);
        this.toastr.error(error.message, 'Login failed');
        this.loading = false;
        this.submitted = true;
    }



}
