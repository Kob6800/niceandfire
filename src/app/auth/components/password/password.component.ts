import {Component, EventEmitter, forwardRef, Input, Output} from '@angular/core';
import {PasswordService} from '../../services/password.service';
import {AbstractControl, ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator} from '@angular/forms';

@Component({
    selector: 'app-password',
    templateUrl: './password.component.html',
    styleUrls: ['./password.component.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => PasswordComponent),
        multi: true
    }, {
        provide: NG_VALIDATORS,
        useExisting: PasswordComponent,
        multi: true,
    }]
})
export class PasswordComponent implements ControlValueAccessor, Validator {
    @Input() fieldSelected: string;
    @Output() blur = new EventEmitter<Event>();
    @Output() focus = new EventEmitter<Event>();
    invalid = false;
    touched = false;
    password = '';
    disabled: boolean;
    passwordStrength = 0;
    messages = [
        'Your word is not save here',
        'Varys will know',
        'This information may come to the wrong hands',
        'The lips are sealed',
        'Secrets are worth more than silver or sapphires'
    ];

    onValueChanged: (newValue: any) => void = () => {
    };
    onTouched: () => void = () => {
    };

    constructor(
        private passwordService: PasswordService
    ) {
    }

    onInput(value: string) {
        this.passwordStrength = this.passwordService.checkPassword(value);
        this.password = value;
        this.checkAndEmit();
    }

    writeValue(obj: any): void {
        this.password = obj || '';
    }

    registerOnChange(fn: any): void {
        this.onValueChanged = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    validate(control: AbstractControl): ValidationErrors | null {
        if (this.isValueValid()) {
            this.invalid = false;
            return null;
        } else {
            this.invalid = true;
            return {
                passwordStrength: true,
                required: this.password.length < 1
            };
        }
    }

    private checkAndEmit() {
        if (this.isValueValid()) {
            this.onValueChanged(this.password);
        } else {
            this.onValueChanged(null);
        }
    }

    private isValueValid() {
        return this.password && this.passwordStrength > 2;
    }

}
