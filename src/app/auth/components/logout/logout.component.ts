import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject<void>();

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit() {
        this.authService.logout().pipe(takeUntil(this.onDestroy$)).subscribe(() => {
            this.router.navigate(['/login']);
        });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

}
