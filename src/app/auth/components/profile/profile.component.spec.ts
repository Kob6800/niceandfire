import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileComponent} from './profile.component';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

describe('ProfileComponent', () => {
    let component: ProfileComponent;
    let fixture: ComponentFixture<ProfileComponent>;
    let authServiceSpy: jasmine.SpyObj<AuthService>;
    let toastrSpy: jasmine.SpyObj<ToastrService>;
    let routerSpy: jasmine.SpyObj<Router>;

    beforeEach(async(() => {
        authServiceSpy = jasmine.createSpyObj('AuthService', ['getFullProfileInformation']);
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
        TestBed.configureTestingModule({
            declarations: [ProfileComponent],
            providers: [
                {provide: AuthService, useValue: authServiceSpy},
                {provide: Router, useValue: routerSpy},
                {provide: ToastrService, useValue: toastrSpy}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProfileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
