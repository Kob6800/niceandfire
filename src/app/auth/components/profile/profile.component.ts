import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {UserProfile} from '../../models/auth.model';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
    @HostBinding('class') hostClass = 'container d-block pt-2';
    loading = true;
    userProfile: UserProfile = null;
    private onDestroy$ = new Subject<void>();

    constructor(
        private authService: AuthService,
        private toastr: ToastrService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        const userInformation$ = this.authService.getFullProfileInformation();

        if (!userInformation$) {
            setTimeout(() => this.toastr.error('Something went wrong, you are not logged in correctly. Pleas log-in again', 'Error'), 1);
            this.router.navigate(['/login']);
            return;
        }

        // profile data not saved in store because it is considered as sensible data
        this.authService.getFullProfileInformation().pipe(takeUntil(this.onDestroy$)).subscribe((userProfile: UserProfile) => {
            if (!userProfile) {
                setTimeout(
                    () => this.toastr.error('Something went wrong, you are not logged in correctly. Pleas log-in again', 'Error'), 1);
                this.router.navigate(['/login']);
            }
            this.loading = false;
            this.userProfile = userProfile;
        });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

}
