import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MockComponent} from 'ng-mocks';
import {PasswordComponent} from '../password/password.component';
import {Router} from '@angular/router';
import {JpPreloadService} from '@jaspero/ng-image-preload';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';

describe('RegisterComponent', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    let authServiceSpy: jasmine.SpyObj<AuthService>;
    let routerSpy: jasmine.SpyObj<Router>;
    let jpPreloadSpy: jasmine.SpyObj<JpPreloadService>;
    let toastrSpy: jasmine.SpyObj<ToastrService>;

    beforeEach(async(() => {
        authServiceSpy = jasmine.createSpyObj('AuthService', ['logout']);
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        jpPreloadSpy = jasmine.createSpyObj('JpPreloadService', ['initialize']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
        TestBed.configureTestingModule({
            declarations: [
                RegisterComponent,
                MockComponent(PasswordComponent)
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule,
            ],
            providers: [
                {provide: AuthService, useValue: authServiceSpy},
                {provide: Router, useValue: routerSpy},
                {provide: JpPreloadService, useValue: jpPreloadSpy},
                {provide: ToastrService, useValue: toastrSpy}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
