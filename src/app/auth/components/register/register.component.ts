import {Component, OnDestroy, OnInit} from '@angular/core';
import {JpPreloadService} from '@jaspero/ng-image-preload';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {UserProfile} from '../../models/auth.model';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
    fieldSelected = '';
    loading = false;
    registerForm: FormGroup;
    errorMessages = {
        required: 'This field is required',
        email: 'Invalid E-Mail',
        passwordStrength: 'Your password is too insecure'
    };
    private submitted = false;
    private onDestroy$ = new Subject<void>();

    constructor(
        private preloadService: JpPreloadService,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private toastr: ToastrService
    ) {
    }

    ngOnInit() {
        this.preloadService.initialize();

        this.registerForm = this.formBuilder.group({
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }


    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    onSubmit() {
        const formFields = this.registerForm.controls;
        this.submitted = true;

        if (this.registerForm.invalid) {
            return;
        }

        const profile: UserProfile = {
            uid: '-1',
            firstname: formFields.firstname.value,
            lastname: formFields.lastname.value,
            email: formFields.email.value,
            travels: [],
        };

        this.loading = true;

        this.authService.register(profile, formFields.password.value)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(this.loginSuccessHandler.bind(this), this.loginErrorHandler.bind(this), () => {
                this.loading = false;
                this.submitted = true;
            });
    }

    private loginSuccessHandler(): void {
        setTimeout(() => this.toastr.success('You are successfully registered', 'Registered'), 1);
        this.router.navigate(['/login']);
    }

    private loginErrorHandler(error: Error): void {
        this.toastr.error(error.message, 'Registration Failed');
        this.loading = false;
        this.submitted = true;
    }

}
