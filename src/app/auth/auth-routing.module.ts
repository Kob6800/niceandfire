import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {LogoutComponent} from './components/logout/logout.component';
import {LoginAuthGuard} from './guards/login-auth.guard';
import {RegisterComponent} from './components/register/register.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [{
    path: 'register',
    canActivate: [LoginAuthGuard],
    component: RegisterComponent,
}, {
    path: 'login',
    canActivate: [LoginAuthGuard],
    component: LoginComponent,
}, {
    path: 'logout',
    component: LogoutComponent,
}, {
    path: 'profile',
    canActivate: [AuthGuard],
    component: ProfileComponent
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {

}
