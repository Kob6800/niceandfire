import {Action, Selector, State, StateContext} from '@ngxs/store';
import {ChangeUser} from './auth.actions';
import {AuthUser} from '../models/auth.model';

export class AuthStateModel {
    public currentUser: AuthUser;
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        currentUser: null,
    }
})
export class AuthState {
    @Selector()
    static currentUser(state: AuthStateModel) {
        return state.currentUser;
    }

    @Action(ChangeUser)
    changeUser(ctx: StateContext<AuthStateModel>, action: ChangeUser) {
        ctx.patchState({
            currentUser: action.user
        });
    }
}
