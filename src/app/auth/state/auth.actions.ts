import {AuthUser} from '../models/auth.model';

export class ChangeUser {
    static readonly type = '[Auth] ChangeUser';

    constructor(public user: AuthUser) {
    }
}
