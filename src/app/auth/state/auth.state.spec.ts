import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {AuthState} from './auth.state';

describe('Main actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([AuthState])]
        }).compileComponents();
        store = TestBed.get(Store);
    }));

});
