import {Travel} from '../../travel-book/models/travel.model';

export class AuthUser {
    uid = '-1';
    email: string;
}

export class UserProfile extends AuthUser {
    firstname: string;
    lastname: string;
    travels?: Travel[];
}
