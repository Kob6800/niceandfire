import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngxs/store';
import {AuthState} from '../state/auth.state';


@Injectable({providedIn: 'root'})
export class LoginAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private store: Store
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.store.selectSnapshot(AuthState.currentUser);
        if (!currentUser) {
            // authorised so return true
            return true;
        }

        this.router.navigate(['/'], {queryParams: {returnUrl: state.url}});
        return false;
    }
}
