export const credentialsMock = {
    email: 'test@test.at',
    password: 'sadfsdfa'
};

export const credUserMock = {
    uid: 'sadfsdfa',
    displayName: null,
    isAnonymous: true,
    email: credentialsMock.email,
};

export const userProfileMock = {
    uid: 'sadfsdfa',
    email: credentialsMock.email,
    firstname: 'Michael',
    lastname: 'Müller',
};

export const firestoreCollection = [[
    {name: 'Polska', uname: 'polska', parent: ''},
    {name: 'Dolnośląskie', uname: 'dolnoslaskie', parent: 'polska'},
    {name: 'Wrocław', uname: 'wroclaw', parent: 'dolnoslaskie'}
]];
