import {TestBed} from '@angular/core/testing';

import {AngularFirestore} from '@angular/fire/firestore';

import {AuthService} from './auth.service';
import {from, of} from 'rxjs';
import {credentialsMock, credUserMock, firestoreCollection, userProfileMock} from './auth-mock.data';
import {AngularFireAuth} from '@angular/fire/auth';
import {Store} from '@ngxs/store';
import {AuthUser} from '../models/auth.model';
import {User} from 'firebase';
import {ChangeUser} from '../state/auth.actions';


describe('AuthService', () => {
    let service: AuthService;
    let angularFirestore: AngularFirestore;
    let storeSpy: jasmine.SpyObj<Store>;
    let store: Store;

    const mockAngularFireAuth = {
        auth: jasmine.createSpyObj('auth', {
            signInWithEmailAndPassword: Promise.resolve({
                user: credUserMock
            }),
            signOut: Promise.resolve(),
            createUserWithEmailAndPassword: Promise.resolve({
                user: credUserMock
            }),
            currentUser: {
                uid: credUserMock.uid
            }

        }),
        authState: of(credUserMock)
    };

    const data = from(firestoreCollection);

    const angularFirestoreDocumentStub = {
        set: jasmine.createSpy('setDocument').and.returnValue(Promise.resolve()),
        get: jasmine.createSpy('getDocument').and.returnValue(Promise.resolve()),
        delete: jasmine.createSpy('deleteDocumentStub').and.returnValue(Promise.resolve()),
    };

    const collectionStub = {
        valueChanges: jasmine.createSpy('valueChanges').and.returnValue(data),
        doc: jasmine.createSpy('collectionDoc').and.returnValue(angularFirestoreDocumentStub)
    };

    const angularFirestoreStub = {
        collection: jasmine.createSpy('firestoreCollection').and.returnValue(collectionStub)
    };

    beforeEach(() => {
        storeSpy = jasmine.createSpyObj<Store>(['dispatch', 'selectSnapshot'] as any);
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AuthService,
                {provide: Store, useValue: storeSpy},
                {provide: AngularFireAuth, useValue: mockAngularFireAuth},
                {provide: AngularFirestore, useValue: angularFirestoreStub}
            ],
        });
        service = TestBed.get(AuthService);
        angularFirestore = TestBed.get(AngularFirestore);
        store = TestBed.get(Store);

    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should login a user', (done) => {
        service.login('michael@mueller.at', 'jlsdfdfsalk').subscribe(cred => {
            expect(cred.user.uid).toEqual(credUserMock.uid);
            expect(cred.user.email).toEqual(credUserMock.email);
            done();
        });
    });

    it('should logout a user', (done) => {
        spyOn(service, 'setCurrentUser');
        service.logout().subscribe(cred => {
            expect(service.setCurrentUser).toHaveBeenCalledTimes(1);
            expect().nothing();
            done();
        });
    });

    it('should register a user', (done) => {
        service.register(userProfileMock, 'secretPassword').subscribe(() => {
            expect(angularFirestoreDocumentStub.set).toHaveBeenCalledTimes(1);
            expect(angularFirestoreDocumentStub.set).toHaveBeenCalledWith(userProfileMock);
            expect(collectionStub.doc).toHaveBeenCalledTimes(1);
            expect(collectionStub.doc).toHaveBeenCalledWith(credUserMock.uid);
            done();
        });
    });

    it('should get full profile information', (done) => {
        const userDocResultStub = {
            data: jasmine.createSpy('data').and.returnValue(userProfileMock)
        };
        angularFirestoreDocumentStub.get.and.returnValue(Promise.resolve(userDocResultStub));
        storeSpy.selectSnapshot.and.returnValue(credentialsMock);
        service.getFullProfileInformation().subscribe((userProfile) => {
            expect(userProfile.uid).toBe(userProfileMock.uid);
            expect(userProfile.firstname).toBe(userProfileMock.firstname);
            expect(userProfile.lastname).toBe(userProfileMock.lastname);
            expect(userProfile.email).toBe(userProfileMock.email);
            expect(userProfile.travels).toBe(undefined);
            expect(angularFirestoreDocumentStub.get).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('should delete current user', (done) => {
        const deleteSpy = jasmine.createSpy('deleteCurrentUser').and.returnValue(Promise.resolve());
        mockAngularFireAuth.auth.currentUser.delete = deleteSpy;

        storeSpy.selectSnapshot.and.returnValue(credentialsMock);
        service.deleteCurrentUser().subscribe((userProfile) => {
            expect(mockAngularFireAuth.auth.currentUser.delete).toHaveBeenCalledTimes(1);
            expect(collectionStub.doc).toHaveBeenCalledWith(credUserMock.uid);
            expect(angularFirestoreDocumentStub.delete).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('should set the current user', () => {
        const firebaseUserMock = jasmine.createSpyObj<User>(['user', 'selectSnapshot'] as any);
        firebaseUserMock.email = credUserMock.email;
        firebaseUserMock.uid = credUserMock.uid;

        service.setCurrentUser(firebaseUserMock);
        const authUser = new AuthUser();
        authUser.email = credUserMock.email;
        authUser.uid = credUserMock.uid;
        expect(storeSpy.dispatch).toHaveBeenCalledWith(new ChangeUser(authUser));
        expect(storeSpy.dispatch).toHaveBeenCalledTimes(1);
        service.setCurrentUser(null);
    });

    it('should "unset" the current user', () => {
        const firebaseUserMock = jasmine.createSpyObj<User>(['user', 'selectSnapshot'] as any);
        firebaseUserMock.email = credUserMock.email;
        firebaseUserMock.uid = credUserMock.uid;

        service.setCurrentUser(null);
        expect(storeSpy.dispatch).toHaveBeenCalledWith(new ChangeUser(null));
        expect(storeSpy.dispatch).toHaveBeenCalledTimes(1);
    });

    it('should console warn error and return null when ' +
        'user is not logged in on get user profile (rest will be handled in component)', () => {
        const consoleSpy = spyOn(console, 'warn');
        storeSpy.selectSnapshot.and.returnValue(null);
        const returnValue = service.getFullProfileInformation();
        expect(returnValue).toBe(null);
        expect(storeSpy.selectSnapshot).toHaveBeenCalledTimes(1);
        expect(consoleSpy).toHaveBeenCalledTimes(1);
        expect(consoleSpy).toHaveBeenCalledWith('User not correctly logged in');
    });

    it('should return null when no user is logged in on deleteCurrentUser', () => {
        const consoleSpy = spyOn(console, 'error');
        const afAuthMock = jasmine.createSpyObj('auth', {
            auth: {
                currentUser: null
            },
        });
        // workaround to dont have to use another test bed for one single test
        (service as any).afAuth = afAuthMock;
        const returnValue = service.deleteCurrentUser();

        expect(returnValue).toBe(null);
        expect(consoleSpy).toHaveBeenCalledTimes(1);
        expect(consoleSpy).toHaveBeenCalledWith('There is no user that can be deleted');
    });

});
