import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {from, Observable} from 'rxjs';
import {flatMap, map, tap} from 'rxjs/operators';
import {User} from 'firebase';

import {AuthUser, UserProfile} from '../models/auth.model';
import {ChangeUser} from '../state/auth.actions';
import {AuthState} from '../state/auth.state';
import UserCredential = firebase.auth.UserCredential;

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    usersRef: AngularFirestoreCollection<AuthUser>;

    constructor(
        private db: AngularFirestore,
        private afAuth: AngularFireAuth,
        private store: Store
    ) {
        this.usersRef = this.db.collection<UserProfile>('users');
    }

    login(email: string, password: string): Observable<UserCredential> {
        return from(this.afAuth.auth.signInWithEmailAndPassword(email, password)).pipe(
            tap(cred => this.setCurrentUser(cred.user))
        );
    }

    logout(): Observable<UserCredential | void> {
        return from(this.afAuth.auth.signOut()).pipe(
            tap(() => this.setCurrentUser(null))
        );
    }

    register(profile: UserProfile, password: string): Observable<void> {
        return from(this.afAuth.auth.createUserWithEmailAndPassword(profile.email, password)).pipe(flatMap(cred => {
            profile.uid = cred.user.uid;
            return from(this.usersRef.doc(profile.uid).set(profile));
        }));
    }

    getFullProfileInformation(): Observable<UserProfile> | null {
        const currentUser = this.store.selectSnapshot(AuthState.currentUser);
        if (!currentUser) {
            console.warn('User not correctly logged in');
            return null;
        }

        return from(this.usersRef.doc(currentUser.uid).get()).pipe(
            map(userDoc => {
                const data = userDoc.data();
                const userProfile = new UserProfile();
                userProfile.firstname = data.firstname;
                userProfile.lastname = data.lastname;
                userProfile.email = data.email;
                userProfile.uid = data.uid;
                userProfile.travels = data.travels;
                return userProfile;
            })
        );
    }

    deleteCurrentUser(): Observable<void> | null {
        const currentUser = this.afAuth.auth.currentUser;

        if (!currentUser) {
            console.error('There is no user that can be deleted');
            return null;
        }

        const currentUserUid = currentUser.uid;

        return from(currentUser.delete()).pipe(
            map(() => {
                this.usersRef.doc(currentUserUid).delete();
            })
        );
    }

    setCurrentUser(user: User): void {
        if (user === null) {
            this.store.dispatch(new ChangeUser(null));
            return;
        }
        const authUser = new AuthUser();
        authUser.email = user.email;
        authUser.uid = user.uid;

        this.store.dispatch(new ChangeUser(authUser));
    }
}
