import {Injectable} from '@angular/core';
import * as passwordLib from 'zxcvbn';

@Injectable({
    providedIn: 'root'
})
export class PasswordService {

    checkPassword(password: string): number {
        if (password !== null && password.length <= 0) {
            return 0;
        }
        const result: passwordLib.ZXCVBNResult = passwordLib(password);
        return result.score;
    }
}
