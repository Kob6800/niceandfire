import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TravelGuideRoutingModule} from './travel-guide-routing.module';
import {RegionOverviewComponent} from './components/region-overview/region-overview.component';
import {RegionDetailComponent} from './components/region-detail/region-detail.component';
import {TravelGuideService} from './services/travel-guide.service';
import {PlaceDetailComponent} from './components/place-detail/place-detail.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    declarations: [
        RegionOverviewComponent,
        RegionDetailComponent,
        PlaceDetailComponent,
    ],
    imports: [
        CommonModule,
        TravelGuideRoutingModule,
        SharedModule
    ],
    providers: [
        TravelGuideService
    ]
})
export class TravelGuideModule {
}
