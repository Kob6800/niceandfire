import {Place} from './place.model';

export class Region extends Place {
    geography: string;
    regionCapital: string;
    culture: string[];
    cities: string[];
    towns: string[];
    castles: string[];
    placesOfNote: string[];
    founder: string[];

    constructor({ _id: id, name, location, religion, rulers,
        geography, regionCapital, culture, cities, towns, castles, placesOfNote, founder }) {
        super({ id, name, location, religion, rulers });
        this.geography = geography;
        this.regionCapital = regionCapital;
        this.culture = [...culture];
        this.towns = [...towns];
        this.castles = [...castles];
        this.placesOfNote = [...placesOfNote];
        this.cities = [...cities];
        this.founder = [...founder];
    }
}

export class RegionQuery {
    name: string;
    capitalCityType: string;
}
