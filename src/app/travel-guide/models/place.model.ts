export abstract class Place {
    religion: string[];
    rulers: string[];
    id: string;
    name: string;
    location: string;

    protected constructor({id, name, location, religion, rulers}) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.religion = [...religion];
        this.rulers = [...rulers];
    }
}
