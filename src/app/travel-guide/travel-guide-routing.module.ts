import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegionOverviewComponent} from './components/region-overview/region-overview.component';
import {RegionDetailComponent} from './components/region-detail/region-detail.component';
import {PlaceDetailComponent} from './components/place-detail/place-detail.component';

const routes: Routes = [
    {
        path: '',
        component: RegionOverviewComponent,
    },
    {
        path: 'region/:name',
        component: RegionDetailComponent,
    },
    {
        path: 'region/:regionName/:type/:name',
        component: PlaceDetailComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

// TODO autentification @see https://github.com/ralphbert/huxe-angular-auth-starter

export class TravelGuideRoutingModule {

}
