import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {TravelGuideState} from './travel-guide.state';

describe('Main actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([TravelGuideState])]
        }).compileComponents();
        store = TestBed.get(Store);
    }));

});
