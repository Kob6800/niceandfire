import {Selector, State} from '@ngxs/store';
import {RegionQuery} from '../models/region.model';

export class TravelGuideStateModel {
    public regionsToChoose: RegionQuery[];
}

@State<TravelGuideStateModel>({
    name: 'travelGuide',
    defaults: {
        regionsToChoose: [
            {
                name: 'Beyond the Wall',
                capitalCityType: ''  // Information unfortionately not given by api, so we have to do it over the store
            },
            {
                name: 'The Crownlands',
                capitalCityType: 'city'
            },
            {
                name: 'The Gift',
                capitalCityType: 'castle'
            },
            {
                name: 'The North',
                capitalCityType: 'castle'
            },
            {
                name: 'The Reach',
                capitalCityType: 'castle'
            },
            {
                name: 'The Riverlands',
                capitalCityType: 'castle'
            }
        ],
    }
})
export class TravelGuideState {
    @Selector()
    static travelRegions(state: TravelGuideStateModel) {
        return state.regionsToChoose.map((region) => region.name);
    }

    @Selector()
    static capitalTypeByCityName(state: TravelGuideStateModel) {
        return (regionName: string) => {
            const regionQuery: RegionQuery = state.regionsToChoose.find((region) => region.name === regionName);
            return regionQuery ? regionQuery.capitalCityType : null;
        };
    }
}
