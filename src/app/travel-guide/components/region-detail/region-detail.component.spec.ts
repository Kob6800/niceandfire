import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionDetailComponent } from './region-detail.component';
import { MockModule } from 'ng-mocks';
import { Store } from '@ngxs/store';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TravelGuideService } from '../../services/travel-guide.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SafeHtmlPipe } from '../../../shared/pipes/save-html.pipe';
import { regionMockTheCrownlands, regionQueryMockTheCrownlands } from '../../services/travel-guide-mock.data';
import { of } from 'rxjs';

describe('RegionDetailComponent', () => {
    let component: RegionDetailComponent;
    let fixture: ComponentFixture<RegionDetailComponent>;
    let store: Store;
    let storeSpy: jasmine.SpyObj<Store>;
    let router: Router;
    let routerSpy: jasmine.SpyObj<Router>;
    let toastrSpy: jasmine.SpyObj<ToastrService>;
    let travelGuide: TravelGuideService;
    let safeHtmlSpy: jasmine.SpyObj<SafeHtmlPipe>;
    let route: ActivatedRoute;

    const travelGuideServiceStub = jasmine.createSpyObj('TravelGuideService', {
        getRegionByName: of(regionMockTheCrownlands),
    });

    const storeStub = jasmine.createSpyObj('Store', {
        select: of((name) => {
            return regionQueryMockTheCrownlands.capitalCityType;
        }),
    });

    beforeEach(() => {
        routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
        safeHtmlSpy = jasmine.createSpyObj('SafeHtmlPipe', ['transform']);
        storeSpy = jasmine.createSpyObj<Store>(['select'] as any);

        TestBed.configureTestingModule({
            imports: [
                MockModule(RouterModule),
                MockModule(RouterTestingModule.withRoutes([])),
            ],
            declarations: [
                RegionDetailComponent,
                SafeHtmlPipe
            ],

            providers: [
                { provide: Router, useValue: routerSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            url: [
                                {
                                    path: 'region/The Crownlands',
                                },
                            ],
                            paramMap: {
                                get: () => {
                                    return 'The Crownlands';
                                }
                            }
                        },
                    }
                },
                { provide: Store, useValue: storeStub },
                { provide: TravelGuideService, useValue: travelGuideServiceStub },
                { provide: ToastrService, useValue: toastrSpy },
                { provide: SafeHtmlPipe, useValue: safeHtmlSpy },
            ]
        })
            .compileComponents();

        router = TestBed.get(Router);
        route = TestBed.get(ActivatedRoute);
        store = TestBed.get(Store);
        travelGuide = TestBed.get(TravelGuideService);
        fixture = TestBed.createComponent(RegionDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create and init correctly with mockup data', () => {
        expect(component).toBeTruthy();
        expect(travelGuide.getRegionByName).toHaveBeenCalledWith('The Crownlands');
        expect(store.select).toHaveBeenCalledTimes(1);
        expect(component.capitalType).toBe('city');
        expect(component.region.regionCapital).toBe('King\'s Landing');
    });

    it('#regionHeader should display name of mockup region', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const regionHeader = regionDetailElement.querySelector('#regionHeader');
        expect(regionHeader).toBeDefined();
        expect(regionHeader.innerText).toContain('The Crownlands');
    });

    it('#regionCapitalTypeChip should display type of mockup regions capital', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const typeChip = regionDetailElement.querySelector('#regionCapitalTypeChip');
        expect(typeChip).toBeDefined();
        expect(typeChip.innerText).toContain('city');
    });

    it('#regionCities should display empty placeholder, as mockup region has no cities', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const citiesRow = regionDetailElement.querySelector('#regionCities');
        const citiesRowDivs = regionDetailElement.querySelector('#regionCities>div');
        expect(citiesRow).toBeDefined();
        expect(citiesRowDivs).toBeDefined();
        expect(citiesRowDivs.innerText).toContain('Nothing');
    });

    it('#regionPlacesOfNote should display all places of note entries of mockup region', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const placesOfNoteRow = regionDetailElement.querySelector('#regionPlacesOfNote');
        const placesOfNoteRowDivs = regionDetailElement.querySelectorAll('#regionPlacesOfNote>div');
        expect(placesOfNoteRow).toBeDefined();
        expect(placesOfNoteRowDivs).toBeDefined();
        expect(placesOfNoteRowDivs.length).toEqual(9);
    });

    it('#regionTowns 2nd child should display mock data town rosby', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const townsRow = regionDetailElement.querySelector('#regionTowns');
        const townsRowDivs = regionDetailElement.querySelectorAll('#regionTowns>div');
        expect(townsRow).toBeDefined();
        expect(townsRowDivs).toBeDefined();
        expect(townsRowDivs[1]).toBeDefined();
        expect(townsRowDivs[1].innerText).toContain('Rosby');
    });

    it('expect mockup region info to be displayed', () => {
        expect(component).toBeDefined();
        const regionDetailElement = fixture.nativeElement;
        const cultureBadges = regionDetailElement.querySelectorAll('.badge-culture');
        const founderBadges = regionDetailElement.querySelectorAll('.badge-founder');
        const religionBadges = regionDetailElement.querySelectorAll('.badge-religion');
        expect(cultureBadges).toBeDefined();
        expect(founderBadges).toBeDefined();
        expect(religionBadges).toBeDefined();
        expect(cultureBadges.length).toBeGreaterThanOrEqual(1);
        expect(founderBadges.length).toBeGreaterThanOrEqual(1);
        expect(religionBadges.length).toBeGreaterThanOrEqual(1);
    });
});
