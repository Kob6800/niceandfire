import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { TravelGuideService } from '../../services/travel-guide.service';
import { Region } from '../../models/region.model';
import { ToastrService } from 'ngx-toastr';
import { TravelGuideState } from '../../state/travel-guide.state';
import { first, map } from 'rxjs/operators';


@Component({
    selector: 'app-region-detail',
    templateUrl: './region-detail.component.html',
    styleUrls: ['./region-detail.component.scss']
})
export class RegionDetailComponent implements OnInit {
    capitalType: string;
    loading = true;
    region: Region;

    constructor(
        private route: ActivatedRoute,
        private travelGuideService: TravelGuideService,
        private toastr: ToastrService,
        private router: Router,
        private store: Store
    ) {
    }

    ngOnInit() {
        this.travelGuideService.getRegionByName(this.route.snapshot.paramMap.get('name'))
            .subscribe(region => {
                this.region = region;
                this.loading = false;
            }, error => {
                setTimeout(() => this.toastr.error(error, 'Could not get Region'), 1);
                this.router.navigate(['../']);
            });
        this.store.select(TravelGuideState.capitalTypeByCityName)
            .pipe(first(), map(fn => {
                return fn(this.route.snapshot.paramMap.get('name'));
            }))
            .subscribe((type => {
                this.capitalType = type;
            }));
    }

}
