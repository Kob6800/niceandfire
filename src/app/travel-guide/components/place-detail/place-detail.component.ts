import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TravelGuideService} from '../../services/travel-guide.service';
import {Place} from '../../models/place.model';
import {BookItem} from '../../../travel-book/models/book-item.model';
import {TravelBookService} from '../../../travel-book/services/travel-book.service';
import {Select} from '@ngxs/store';
import {TravelBookState} from '../../../travel-book/state/travel-book.state';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {HtmlEscapeService} from '../../../shared/services/html-escape.service';

@Component({
    selector: 'app-book-item-detail',
    templateUrl: './place-detail.component.html',
    styleUrls: ['./place-detail.component.scss']
})
export class PlaceDetailComponent implements OnInit, OnDestroy {
    @Select(TravelBookState.bookedItems) bookedItems$: Observable<BookItem[]>;
    onDestroy$ = new Subject<void>();
    type: string;
    place: Place;
    loading = true;
    isBooked = false;

    constructor(
        private route: ActivatedRoute,
        private travelGuideService: TravelGuideService,
        private travelBookService: TravelBookService,
        private router: Router,
        private toastr: ToastrService,
        private htmlEscapeService: HtmlEscapeService
    ) {
    }

    ngOnInit() {
        this.setBookItem = this.setBookItem.bind(this);
        this.handleLoadingError = this.handleLoadingError.bind(this);
        const name = this.htmlEscapeService.decode(this.route.snapshot.paramMap.get('name'));
        this.type = this.htmlEscapeService.decode(this.route.snapshot.paramMap.get('type'));

        // travel details and regions not changeable so no need to use a store for them
        switch (this.type) {
            case 'castle':
                this.travelGuideService.getCastleByName(name).subscribe(this.setBookItem, this.handleLoadingError);
                break;
            case 'town':
                this.travelGuideService.getTownByName(name).subscribe(this.setBookItem, this.handleLoadingError);
                break;
            case 'city':
                this.travelGuideService.getCityByName(name).subscribe(this.setBookItem, this.handleLoadingError);
                break;
            default:
                this.router.navigate(['/notFound'], {queryParams: {returnUrl: this.route.url}});
                break;
        }
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    addBookItem(item) {
        this.travelBookService.addBookItem(item);
    }

    private setBookItem(bookItem: BookItem) {
        this.place = bookItem;
        this.loading = false;

        this.checkIfAlreadyBooked();
    }

    private handleLoadingError(error) {
        setTimeout(() => this.toastr.error(error, `Could not load ${this.type}`), 1);
        this.router.navigate(['../../']);
    }

    private checkIfAlreadyBooked() {
        this.bookedItems$.pipe(takeUntil(this.onDestroy$))
            .subscribe(bookedItems => {
                this.isBooked = !!bookedItems.find(item => this.place && this.place.id === item.id);
            });
    }
}
