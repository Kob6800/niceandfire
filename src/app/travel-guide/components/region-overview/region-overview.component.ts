import {Component} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';
import {TravelGuideState} from '../../state/travel-guide.state';

@Component({
    selector: 'app-region-overview',
    templateUrl: './region-overview.component.html',
    styleUrls: ['./region-overview.component.scss']
})
export class RegionOverviewComponent {
    @Select(TravelGuideState.travelRegions) regions$: Observable<string[]>;
}
