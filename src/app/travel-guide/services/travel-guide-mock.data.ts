export const regionMockTheCrownlands = {
    // from place
    id: '5cc0747104e71a0010b85899',
    name: 'The Crownlands',
    location: 'Westeros',
    rulers: [
        'King of the Andals, the Rhoynar, and the First Men',
        'House Lannister',
        'House Baratheon of King\'s Landing',
        'House Targaryen'
    ],
    religion: [
        'Faith of the Seven'
    ],

    // from region
    // tslint:disable-next-line: max-line-length
    geography: 'Plains and river valleys, with forests in the south and in Crackclaw Point to the north; temperate/Mediterranean climate on the coast around King\'s Landing',
    regionCapital: 'King\'s Landing',
    culture: [
        'Andals'
    ],
    cities: [
    ],
    towns: [
        'Duskendale',
        'Rosby',
        'Rook\'s Rest',
        'Sow\'s Horn'
    ],
    castles: [
        'Antlers',
        'Red Keep',
        'Dragonstone (castle)',
        'Dyre Den',
        'The Whispers',
        'Castle Stokeworth',
        'Sharp Point',
        'Stonedance'
    ],
    founder: [
        'Aegon I Targaryen'
    ],
    placesOfNote: [
        'Blackwater Bay',
        'Blackwater Rush',
        'Claw Isle',
        'Crackclaw Point',
        'Kingswood',
        'Kingsroad',
        'Dragonstone (island)',
        'Driftmark',
        'Spears of the Merling King'
    ],
};

export const regionMockTheNorth = {
    // from place
    id: '5cc0747204e71a0010b858ac',
    name: 'The North',
    location: 'Seven Kingdoms',
    rulers: [
        'Jon Snow',
        'House Stark',
        'House Bolton'
    ],
    religion: [
        'Old Gods of the Forest',
        'Faith of the Seven'
    ],

    // from region
    // tslint:disable-next-line: max-line-length
    geography: 'Rivers and plains, with forests and mountains in the northwest; temperate to sub-arctic.Swamps in the Neck along southern border.',
    regionCapital: 'Winterfell',
    culture: [
        'Northmen',
        'Andals',
        'Crannogmen',
        'The Neck',
        'First Men',
        'Giants',
        'Children of the Forest',
        'Andal Invasion'
    ],
    cities: [
        'White Harbor'
    ],
    towns: [
        'Barrowton'
    ],
    castles: [
        'Castle Cerwyn',
        'Deepwood Motte',
        'Flint\'s Finger',
        'Greywater Watch',
        'Highpoint',
        'Hornwood',
        'Ironrath',
        'Karhold',
        'Last Hearth',
        'Moat Cailin',
        'Oldcastle',
        'Queenscrown',
        'Ramsgate',
        'Rillwater Crossing',
        'The Dreadfort',
        'Torrhen\'s Square',
        'Widow\'s Watch',
        'House Dormund'
    ],
    founder: [
        'Brandon Stark (the Builder)'
    ],
    placesOfNote: [
        'Barrowlands',
        'Bay of Ice',
        'Bay of Seals',
        'Bear Island',
        'The Bite',
        'Blazewater Bay',
        'Broken Branch',
        'Cape Kraken',
        'The Gift',
        'The Grey Cliffs',
        'Ironman\'s Bay',
        'Last River',
        'Long Lake',
        'The Neck',
        'The Rills',
        'Sea Dragon Point',
        'Skagos',
        'Skane',
        'Stony Shore',
        'The Wall',
        'Weeping Water',
        'White Knife',
        'Wolfswood'
    ],
};

export const regionQueryMockTheCrownlands = {
    name: 'The Crownlands',
    capitalCityType: 'city'
};
export const regionQueryMockTheNorth = {
    name: 'The North',
    capitalCityType: 'castle'
};
