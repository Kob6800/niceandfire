import {TestBed} from '@angular/core/testing';

import {TravelGuideService} from './travel-guide.service';
import {HttpClient} from '@angular/common/http';

describe('TravelGuideService', () => {
    let httpMock: jasmine.SpyObj<HttpClient>;

    beforeEach(() => {
        httpMock = jasmine.createSpyObj('HttpClient', ['get']);
        TestBed.configureTestingModule({
            providers: [
                TravelGuideService,
                {provide: HttpClient, useValue: httpMock},
            ]
        });
    });

    it('should be created', () => {
        const service: TravelGuideService = TestBed.get(TravelGuideService);
        expect(service).toBeTruthy();
    });
});
