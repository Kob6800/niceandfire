import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {map} from 'rxjs/operators';
import {Region} from '../models/region.model';
import {Castle, City, Town} from '../../travel-book/models/book-item.model';

@Injectable()
export class TravelGuideService {

    constructor(
        private http: HttpClient
    ) {
    }

    public getRegionByName(name: string): Observable<Region> {
        return this.get(`/show/regions/${name}`).pipe(
            map(data => new Region(data[0]))
        );
    }

    public getCityByName(name: string): Observable<City> {
        return this.get(`/show/cities/${name}`).pipe(
            map(data => new City(data[0]))
        );
    }

    public getTownByName(name: string): Observable<Town> {
        return this.get(`/show/towns/${name}`).pipe(
            map(data => new Town(data[0]))
        );
    }

    public getCastleByName(name: string): Observable<Castle> {
        return this.get(`/show/castles/${name}`).pipe(
            map(data => new Castle(data[0]))
        );
    }

    private get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http.get(`${environment.gotInfoApiUrl}${path}`, {params});
    }

}


