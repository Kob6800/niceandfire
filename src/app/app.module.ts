import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutModule} from './layout/layout.module';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from './shared/shared.module';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {environment} from '../environments/environment.prod';
import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsStoragePluginModule} from '@ngxs/storage-plugin';
import {AuthState} from './auth/state/auth.state';
import {TravelGuideState} from './travel-guide/state/travel-guide.state';
import {HomeModule} from './home/home.module';
import {TravelBookState} from './travel-book/state/travel-book.state';

// @see https://angular-templates.io/tutorials/about/firebase-authentication-with-angular

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        NgxsModule.forRoot([AuthState, TravelGuideState, TravelBookState], {
            developmentMode: !environment.production
        }),
        NgxsStoragePluginModule.forRoot(),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        LayoutModule,
        SharedModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        AngularFirestoreModule,
        AngularFireAuthModule,
        BrowserAnimationsModule,
        HomeModule,
        ToastrModule.forRoot(),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
