import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Select} from '@ngxs/store';

import {AuthUser} from '../../../auth/models/auth.model';
import {AuthState} from '../../../auth/state/auth.state';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    @Select(AuthState.currentUser) currentUser$: Observable<AuthUser>;

    constructor() {
    }

    ngOnInit() {
    }

}
