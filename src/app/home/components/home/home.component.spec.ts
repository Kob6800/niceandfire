import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {RouterTestingModule} from '@angular/router/testing';
import {Store} from '@ngxs/store';
import {of} from 'rxjs';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    let storeSpy: jasmine.SpyObj<Store>;
    let store: Store;

    beforeEach(async(() => {
        storeSpy = jasmine.createSpyObj<Store>(['dispatch', 'selectSnapshot'] as any);
        TestBed.configureTestingModule({
            declarations: [
                HomeComponent,
            ],
            imports: [
                RouterTestingModule
            ],
            providers: [{provide: Store, useValue: storeSpy}]
        })
            .compileComponents();
        store = TestBed.get(Store);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        Object.defineProperty(component, 'currentUser$', {writable: true});
        component.currentUser$ = of({uid: 'sdfasdafsadf', email: 'test@test.at'});
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
