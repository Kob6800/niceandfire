import {TestBed} from '@angular/core/testing';

import {HtmlEscapeService} from './html-escape.service';

describe('HtmlEscapeService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: HtmlEscapeService = TestBed.get(HtmlEscapeService);
        expect(service).toBeTruthy();
    });
});
