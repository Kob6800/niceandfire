import {Injectable} from '@angular/core';
import * as htmlDecode from 'decode-html';

@Injectable({
    providedIn: 'root'
})
export class HtmlEscapeService {
    decode(str: string): string {
        return htmlDecode(str);
    }
}
