import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kebapCase'
})
export class KebapCasePipe implements PipeTransform {

    transform(str: string): string {
        return str.replace(/([a-z])([A-Z])/g, '$1-$2')
            .replace(/[\s_]+/g, '-')
            .toLowerCase();
    }

}
