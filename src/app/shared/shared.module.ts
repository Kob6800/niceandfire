import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {NetworkErrorInterceptor} from './interceptors/network-error.interceptor';
import {RouterModule} from '@angular/router';
import {SafeHtmlPipe} from './pipes/save-html.pipe';
import {KebapCasePipe} from './pipes/kebap-case.pipe';

@NgModule({
    declarations: [
        NotFoundComponent,
        SafeHtmlPipe,
        KebapCasePipe
    ],
    imports: [
        CommonModule,
        RouterModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NetworkErrorInterceptor,
            multi: true
        }
    ],
    exports: [
        NotFoundComponent,
        SafeHtmlPipe,
        KebapCasePipe
    ]
})
export class SharedModule {
}
