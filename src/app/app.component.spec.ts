import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';

import {LayoutModule} from './layout/layout.module';
import {HomeModule} from './home/home.module';
import {SharedModule} from './shared/shared.module';
import {MockModule} from 'ng-mocks';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MockModule(RouterTestingModule),
        MockModule(LayoutModule),
        MockModule(HomeModule),
        MockModule(SharedModule)
      ],
      declarations: [
        AppComponent,

      ],
    }); // .compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
});
