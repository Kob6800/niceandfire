import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './shared/components/not-found/not-found.component';
import {HomeComponent} from './home/components/home/home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent, // no need to lazy laod home component
    },
    {
        path: 'travelGuide',
        loadChildren: './travel-guide/travel-guide.module#TravelGuideModule',
    },
    {
        path: 'book',
        loadChildren: './travel-book/travel-book.module#TravelBookModule',
    },
    {
        path: '',
        loadChildren: './auth/auth.module#AuthModule',
    },
    {
        path: '**', // default route must stand on bottom
        component: NotFoundComponent, // TODO 404 not found component
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
