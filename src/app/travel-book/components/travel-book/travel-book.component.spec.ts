import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TravelBookComponent} from './travel-book.component';
import {MockComponent} from 'ng-mocks';
import {AngularFontAwesomeComponent} from 'angular-font-awesome';
import {NgxSmartModalComponent, NgxSmartModalService} from 'ngx-smart-modal';
import {Store} from '@ngxs/store';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TravelBookService} from '../../services/travel-book.service';

describe('TravelBookComponent', () => {
    let component: TravelBookComponent;
    let fixture: ComponentFixture<TravelBookComponent>;
    let storeSpy: jasmine.SpyObj<Store>;
    let store: Store;
    let toastrSpy: jasmine.SpyObj<ToastrService>;
    let routerSpy: jasmine.SpyObj<Router>;
    let travelBookServiceSpy: jasmine.SpyObj<TravelBookService>;
    let mondalSpy: jasmine.SpyObj<NgxSmartModalComponent>;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
        travelBookServiceSpy = jasmine.createSpyObj('TravelBookService', ['removeBookItem', 'clearBookTiems', 'bookTravel']);
        mondalSpy = jasmine.createSpyObj('NgxSmartModalComponent', ['close']);
        storeSpy = jasmine.createSpyObj<Store>(['dispatch', 'selectSnapshot'] as any);
        TestBed.configureTestingModule({
            declarations: [
                TravelBookComponent,
                MockComponent(AngularFontAwesomeComponent),
                MockComponent(NgxSmartModalComponent)
            ],
            providers: [
                {provide: Store, useValue: storeSpy},
                {provide: Router, useValue: routerSpy},
                {provide: ToastrService, useValue: toastrSpy},
                {provide: TravelBookService, useValue: travelBookServiceSpy},
                {provide: NgxSmartModalService, useValue: mondalSpy},
            ]
        })
            .compileComponents();
        store = TestBed.get(Store);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TravelBookComponent);
        component = fixture.componentInstance;
        Object.defineProperty(component, 'currentUser$', {writable: true});
        component.currentUser$ = of({uid: 'sdfasdafsadf', email: 'test@test.at'});
        Object.defineProperty(component, 'bookedItems$', {writable: true});
        component.bookedItems$ = of([]);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
