import {Component, OnDestroy} from '@angular/core';
import {Select} from '@ngxs/store';
import {TravelBookState} from '../../state/travel-book.state';
import {Observable, Subject} from 'rxjs';
import {BookItem} from '../../models/book-item.model';
import {TravelBookService} from '../../services/travel-book.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {AuthUser} from '../../../auth/models/auth.model';
import {AuthState} from '../../../auth/state/auth.state';

@Component({
    selector: 'app-travel-book',
    templateUrl: './travel-book.component.html',
    styleUrls: ['./travel-book.component.scss']
})
export class TravelBookComponent implements OnDestroy {
    @Select(TravelBookState.bookedItems) bookedItems$: Observable<BookItem[]>;
    @Select(AuthState.currentUser) currentUser$: Observable<AuthUser>;
    onDestroy$ = new Subject<void>();
    loadingBooking = false;

    constructor(
        private travelBookService: TravelBookService,
        private modalService: NgxSmartModalService,
        private toastr: ToastrService,
        private router: Router
    ) {
    }

    ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }

    removeItem(item) {
        this.travelBookService.removeBookItem(item);
    }

    bookTravel() {
        this.loadingBooking = true;
        this.travelBookService.bookTravel().pipe(takeUntil(this.onDestroy$)).subscribe(() => {
            setTimeout(() => this.toastr.success('You have successfully booked your travel thought Westeros', 'Congratulation'), 1);
            this.router.navigate(['/profile']);
        }, (error: Error) => {
            setTimeout(() => this.toastr.error(error.message, 'Booking Error', {timeOut: 7000}), 1);
            this.router.navigate(['/login']);
        });
        this.modalService.close('bookTravelModal');
    }

    clearTravel() {
        this.travelBookService.clearBookTiems();
        this.modalService.close('clearTravelModal');
    }

}
