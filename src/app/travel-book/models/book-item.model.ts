import {Place} from '../../travel-guide/models/place.model';

export abstract class BookItem extends Place {
    type: string;
}

export class City extends BookItem {
    placesOfNote: string[];
    founder: string[];

    constructor({_id: id, name, location, religion, rulers, founder, placesOfNote}) {
        super({id, name, location, religion, rulers});
        this.placesOfNote = [...placesOfNote];
        this.founder = [...founder];
        this.type = 'city';
    }
}

export class Town extends BookItem {
    placesOfNote: string[];

    constructor({_id: id, name, location, religion, rulers}) {
        super({id, name, location, religion, rulers});
        this.type = 'town';
    }
}

export class Castle extends BookItem {
    placesOfNote: string[];

    constructor({_id: id, name, location, religion, rulers}) {
        super({id, name, location, religion, rulers});
        this.type = 'castle';
    }

}
