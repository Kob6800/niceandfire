import {BookItem} from './book-item.model';

export class Travel {
    uid: string;
    bookedItems: BookItem[];
}
