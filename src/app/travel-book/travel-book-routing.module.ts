import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TravelBookComponent} from './components/travel-book/travel-book.component';

const routes: Routes = [
    {
        path: 'checkout',
        component: TravelBookComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class TravelBookRoutingModule {

}
