import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TravelBookComponent} from './components/travel-book/travel-book.component';
import {TravelBookRoutingModule} from './travel-book-routing.module';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgxSmartModalModule} from 'ngx-smart-modal';

@NgModule({
    declarations: [
        TravelBookComponent
    ],
    imports: [
        CommonModule,
        TravelBookRoutingModule,
        AngularFontAwesomeModule,
        NgxSmartModalModule.forChild()
    ]
})
export class TravelBookModule {
}
