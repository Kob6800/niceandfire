import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {TravelBookState} from './travel-book.state';

describe('Main actions', () => {
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([TravelBookState])]
        }).compileComponents();
        store = TestBed.get(Store);
    }));

});
