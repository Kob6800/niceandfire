import {Action, Selector, State, StateContext} from '@ngxs/store';
import {BookItem} from '../models/book-item.model';
import {AddBookItem, RemoveBookItem, ResetBookItems, UnsetError} from './travel-book.actions';

export class TravelBookStateModel {
    public bookedItems: BookItem[];
    public error: string;
}

@State<TravelBookStateModel>({
    name: 'travelBook',
    defaults: {
        bookedItems: [],
        error: null
    }
})
export class TravelBookState {
    @Selector()
    static error(state: TravelBookStateModel): string {
        return state.error;
    }

    @Selector()
    static bookedItemsNumber(state: TravelBookStateModel): number {
        return state.bookedItems.length;
    }

    @Selector()
    static bookedItems(state: TravelBookStateModel): BookItem[] {
        return state.bookedItems;
    }

    @Action(AddBookItem)
    addBookItem(ctx: StateContext<TravelBookStateModel>, action: AddBookItem): void {
        const bookedItems = ctx.getState().bookedItems;

        if (bookedItems.find(item => item.id === action.item.id)) {
            ctx.patchState({
                error: 'The item was already added to the Travel'
            });
            return;
        }

        const newBookedItems = [...bookedItems, action.item];
        ctx.patchState({
            bookedItems: newBookedItems
        });
    }

    @Action(RemoveBookItem)
    removeBookItem(ctx: StateContext<TravelBookStateModel>, action: RemoveBookItem): void {
        const bookedItems = ctx.getState().bookedItems;


        if (!bookedItems.find(item => item.id === action.itemId)) {
            ctx.patchState({
                error: `The item could not get removed from your travel`
            });
            return;
        }

        const newBookedItems = bookedItems.filter(item => item.id !== action.itemId);
        ctx.patchState({
            bookedItems: newBookedItems
        });
    }

    @Action(UnsetError)
    unsetError(ctx: StateContext<TravelBookStateModel>): void {
        ctx.patchState({
            error: null
        });
    }

    @Action(ResetBookItems)
    resetBookItems(ctx: StateContext<TravelBookStateModel>): void {
        ctx.patchState({
            bookedItems: []
        });
    }

}
