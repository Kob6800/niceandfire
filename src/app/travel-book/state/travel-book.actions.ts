import {BookItem} from '../models/book-item.model';

export class AddBookItem {
    static readonly type = '[Travel Book] AddBookItem';

    constructor(public item: BookItem) {
    }
}


export class RemoveBookItem {
    static readonly type = '[Travel Book] RemoveBookItem';

    constructor(public itemId: string) {
    }
}

export class UnsetError {
    static readonly type = '[Travel Book] UnsetError';
}

export class ResetBookItems {
    static readonly type = '[Travel Book] ResetBookItems';
}

