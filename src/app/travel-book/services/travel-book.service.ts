import {Injectable} from '@angular/core';
import {BookItem} from '../models/book-item.model';
import {Select, Store} from '@ngxs/store';
import {AddBookItem, RemoveBookItem, ResetBookItems, UnsetError} from '../state/travel-book.actions';
import {ToastrService} from 'ngx-toastr';
import {TravelBookState} from '../state/travel-book.state';
import {from, Observable, throwError} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthState} from '../../auth/state/auth.state';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Travel} from '../models/travel.model';
import {Guid} from 'guid-typescript';


@Injectable({
    providedIn: 'root'
})
export class TravelBookService {
    @Select(TravelBookState.error) error$: Observable<string>;

    constructor(
        private store: Store,
        private toastr: ToastrService,
        private router: Router,
        private db: AngularFirestore,
    ) {
        this.initService();
    }


    public addBookItem(item: BookItem): void {
        this.store.dispatch(new AddBookItem(item));
    }

    public removeBookItem(item: BookItem): void {
        this.store.dispatch(new RemoveBookItem(item.id));
    }

    public clearBookTiems(): void {
        this.store.dispatch(new ResetBookItems());
    }

    public bookTravel(): Observable<void> {
        const currentUser = this.store.selectSnapshot(AuthState.currentUser);
        if (currentUser == null) {
            const error = throwError(
                new Error('Only logged in users can book travels, you are currently not logged in, please register or log in first.')
            );
            return error;
        }
        const travel = new Travel();
        travel.uid = Guid.create().toString();
        travel.bookedItems = this.store.selectSnapshot(TravelBookState.bookedItems);

        const userRef: AngularFirestoreDocument = this.db.doc(`users/${currentUser.uid}`);
        const key = `travels.${travel.uid}`;
        return from(userRef.set({
                travels: [JSON.parse(JSON.stringify(travel))]
            }, {merge: true}
        )).pipe(map(() => {
            this.store.dispatch(new ResetBookItems());
            return;
        }));

    }

    private initService() {
        this.error$.pipe(filter(error => !!error)).subscribe(error => {
            this.toastr.error(error, 'Booking Error');
            this.store.dispatch(new UnsetError());
        });
    }
}
