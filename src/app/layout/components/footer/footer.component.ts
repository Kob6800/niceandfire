import {Component, HostBinding} from '@angular/core';

@Component({
    selector: 'app-layout-footer',
    templateUrl: './footer.component.html',
})
export class FooterComponent {
    @HostBinding('class') hostClass = 'bg-light d-block';
}
