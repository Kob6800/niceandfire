import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NavbarComponent} from './navbar.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MockComponent} from 'ng-mocks';
import {AngularFontAwesomeComponent} from 'angular-font-awesome';
import {Store} from '@ngxs/store';
import {of} from 'rxjs';

describe('NavbarComponent', () => {
    let component: NavbarComponent;
    let fixture: ComponentFixture<NavbarComponent>;
    let storeSpy: jasmine.SpyObj<Store>;
    let store: Store;

    beforeEach(async(() => {
        storeSpy = jasmine.createSpyObj<Store>(['dispatch', 'selectSnapshot'] as any);
        TestBed.configureTestingModule({
            declarations: [
                NavbarComponent,
                MockComponent(AngularFontAwesomeComponent)
            ],
            imports: [
                RouterTestingModule,
            ],
            providers: [
                {provide: Store, useValue: storeSpy},
            ]
        })
            .compileComponents();
        store = TestBed.get(Store);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavbarComponent);
        component = fixture.componentInstance;
        Object.defineProperty(component, 'currentUser$', {writable: true});
        component.currentUser$ = of({uid: 'sdfasdafsadf', email: 'test@test.at'});
        Object.defineProperty(component, 'bookedItemsNumber$', {writable: true});
        component.bookedItemsNumber$ = of(10);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
