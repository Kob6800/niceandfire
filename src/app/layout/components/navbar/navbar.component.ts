import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthUser} from '../../../auth/models/auth.model';
import {Select} from '@ngxs/store';
import {AuthState} from '../../../auth/state/auth.state';
import {TravelBookState} from '../../../travel-book/state/travel-book.state';

@Component({
    selector: 'app-layout-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
    @Select(AuthState.currentUser) currentUser$: Observable<AuthUser>;
    @Select(TravelBookState.bookedItemsNumber) bookedItemsNumber$: Observable<number>;
    isCollapsed = false;
}
