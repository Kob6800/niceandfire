import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {CollapseModule} from 'ngx-bootstrap';
import {AppRoutingModule} from '../app-routing.module';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

@NgModule({
    declarations: [
        FooterComponent,
        HeaderComponent,
        NavbarComponent
    ],
    imports: [
        CommonModule,
        CollapseModule,
        AppRoutingModule,
        AngularFontAwesomeModule
    ],
    exports: [
        FooterComponent,
        HeaderComponent
    ]
})
export class LayoutModule {
}
