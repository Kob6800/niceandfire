# Niceandfire, a GOT city guide

This is a sample project about Angular done during a lab course at [FH Hagenberg](https://www.fh-ooe.at/campus-hagenberg/) and was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
The site utilzes calls to the public avialable [GoT API](https://api.got.show/doc/) in order to visualize different interactive views on provided data.

## Team

Bernhard Wagner (S1810629012) \ Daniel Hölbling (S1810629004)

## Goal

The Goal is to create an simple application that provides information about certain places in westeros.
You should be able to log in and create your individual travel with the provided information.

## Production Link
[https://nice-and-fire.netlify.com](https://nice-and-fire.netlify.com)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
