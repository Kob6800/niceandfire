import { AppPage } from './app.po';
import { browser, logging, element, by, ElementFinder } from 'protractor';

describe('Niceandfire', () => {
  let page: AppPage;
  const homeBtn2Guide = element(by.id('toGuide'));
  const guideHeader = element(by.id('guideHeader'));
  const regionHeader = element(by.id('regionHeader'));
  const regionBtn2Guide = element(by.id('regionGoGuide'));
  const placeHeader = element(by.id('placeHeader'));
  const placeBtn2Region = element(by.id('placeGoRegion'));
  const placeBtn2Guide = element(by.id('placeGoGuide'));
  const checkoutHeader = element(by.id('checkoutHeader'));
  const loginHeader = element(by.id('loginHeader'));
  const registerHeader = element(by.id('registerHeader'));


  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('should display welcome message', () => {
    expect(page.getTitleText()).toEqual('Welcome Travelor!');
  });

  it('should go to region overview and read the header', () => {
    homeBtn2Guide.click();
    expect(guideHeader.getText()).toContain('Choose a region');
  });
  it('should go to the north region and read the header', () => {
    page.navigateToSlug('/travelGuide/region/The%20North');
    expect(regionHeader.getText()).toContain('The North');
  });
  it('should go to the Barrowton, read the header and add it to myTravel', () => {
    page.navigateToSlug('/travelGuide/region/The%20North/town/Barrowton');
    expect(placeHeader.getText()).toContain('Barrowton');
  });
  it('should go to the Barrowton, go 2 region go 2 overview', () => {
    page.navigateToSlug('/travelGuide/region/The%20North/town/Barrowton');
    expect(placeHeader.getText()).toContain('Barrowton');
    // go 1 up
    placeBtn2Region.click();
    expect(regionHeader.getText()).toContain('The North');
    // go 1 up
    regionBtn2Guide.click();
    expect(guideHeader.getText()).toContain('Choose a region');
  });
  it('should go to the Barrowton, go 2 overview', () => {
    page.navigateToSlug('/travelGuide/region/The%20North/town/Barrowton');
    expect(placeHeader.getText()).toContain('Barrowton');
    // go 2 up
    placeBtn2Guide.click();
    expect(guideHeader.getText()).toContain('Choose a region');
  });

  it('should go to the checkout and read the header', () => {
    page.navigateToSlug('/book/checkout');
    expect(checkoutHeader.getText()).toContain('My travel');
  });
  it('should go to the login and read the header', () => {
    page.navigateToSlug('/login');
    expect(loginHeader.getText()).toContain('Login');
  });
  it('should go to the register and read the header', () => {
    page.navigateToSlug('/register');
    expect(registerHeader.getText()).toContain('Register');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
